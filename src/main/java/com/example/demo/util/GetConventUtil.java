package com.example.demo.util;

import java.util.*;

/**
 * @author : Mr Li
 * @description : 将对象拼接成Get路径头
 */
public class GetConventUtil {

    private GetConventUtil() {
    }

    /**
     * 使用 Map按key进行排序
     *
     * @param map f
     * @return f
     */
    public static Map<String, Object> sortMapByKey(Map<String, Object> map) {
        if (map == null || map.isEmpty()) {
            return Collections.emptyMap();
        }
        return new TreeMap<>(map);
    }

    /**
     * 签名
     */
    public static String sign(Map<String, Object> map) {
        if (map.isEmpty()) {
            return "";
        }
        map = sortMapByKey(map);
        List<Object> list = new ArrayList<>();
        var str = new StringBuilder();
        for (Map.Entry<String,Object> entry : map.entrySet()) {
            if (null != entry.getValue()) {
                list.add(entry.getKey() + "=" + entry.getValue());
            }
        }
        int leng = list.size() - 1;
        for (var i = 0; i < leng; i++) {
            str.append(list.get(i)).append("&");
            //拼索要加密的字符串格式
        }
        str.append(list.get(leng));
        return str.toString();
    }

}
