package com.example.demo.util;

import com.example.demo.vo.JavaMailVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * @author : Mr Li
 * @description : 发送邮件
 */
@Slf4j
@Service
public class JavaEmailUtil {

    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    public JavaMailSender javaMailSender;

    public void commonEmail(JavaMailVO toEmail) {
        //创建简单邮件消息
        SimpleMailMessage message = new SimpleMailMessage();
        //谁发的
        message.setFrom(from);
        //谁要接收
        message.setTo(toEmail.getTos());
        //邮件标题
        message.setSubject(toEmail.getSubject());
        //邮件内容
        message.setText(toEmail.getContent());
        try {
            log.info("开始发送邮件{}", message);
            javaMailSender.send(message);
        } catch (MailException e) {
            log.error(e.getMessage());
        }
    }
}
