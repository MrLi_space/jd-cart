package com.example.demo.jobhandler;

import cn.hutool.http.HttpUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author : Mr Li
 * @description : 简单定时器接口
 */
@Slf4j
@Component
public class SimpleJobHandler {

    @Value("${server.port}")
    private String port;

    @XxlJob("simpleJobHandler")
    public ReturnT<String> simpleJobHandler(String param){
        log.info("simpleJobHandler-->>-->>-->>init.");
        var response = HttpUtil.createGet("http://127.0.0.1:"+port+"/jd-controller/getInfo").execute();
        log.info("simpleJobHandler-->>-->>-->>stop.{}",response);
        return ReturnT.SUCCESS;
    }

    @XxlJob("jobHandlerCreateOrder")
    public ReturnT<String> jobHandlerCreateOrder(String param){
        log.info("jobHandlerCreateOrder-->>-->>-->>init.");
        var response = HttpUtil.createGet("http://127.0.0.1:"+port+"/jd-controller/createOrder").execute();
        log.info("jobHandlerCreateOrder-->>-->>-->>stop.{}",response);
        return ReturnT.SUCCESS;
    }
}
