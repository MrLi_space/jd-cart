package com.example.demo.exception;

import com.example.demo.common.R;
import com.example.demo.common.RE;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author ：Mr Li
 * @description ：全局异常处理器
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionAdvice {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(WorldLetException.class)
    public R<String> demoException(WorldLetException e) {
        return R.failed(e.getCode(), e.getMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public R<String> httpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        log.error(e.getMethod());
        return R.failed(RE.FAILED_REQUEST_NOT_SUPPORTED);
    }

    /**
     * 其他未知异常捕获后统一处理。
     *
     * @param e 未知异常
     * @return 异常提示信息
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public R<String> exception(Exception e) {
        log.error(e.getMessage(), e);
        return R.failed();
    }
}