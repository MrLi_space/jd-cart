package com.example.demo.exception;

import com.example.demo.common.RE;
import lombok.Getter;

/**
 * @author ：Mr Li
 * @description ： demo 异常
 */
public class WorldLetException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    @Getter
    private final int code;

    public WorldLetException(RE re) {
        super(re.getMsg());
        this.code = re.getCode();
    }

    public WorldLetException(String msg) {
        super(msg);
        this.code = RE.FAILED.getCode();
    }
}
