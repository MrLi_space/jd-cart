package com.example.demo.controller;

import com.example.demo.common.R;
import com.example.demo.service.JDServiceImpl;
import com.example.demo.vo.RedisPostVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author : Mr Li
 * @description : 监听购物车商品状态
 */
@Slf4j
@RestController
@RequestMapping("/jd-controller")
public class JDController {

    @Autowired
    public JDServiceImpl service;


    @PostMapping("/insertUserMail")
    public R<Boolean> insertUserMail(@RequestBody RedisPostVO vo) {
        final Boolean result = service.insertUserMail(vo.getTos());
        return R.ok(result);
    }

    @PostMapping("/insertUserCookie")
    public R<Void> insertUserCookie(@RequestBody RedisPostVO vo ) {
        service.insertUserCookie(vo.getCookie());
        return R.ok();
    }

    @GetMapping("/getInfo")
    public R<Void> resolveCart() {
        service.resolveCart();
        return R.ok();
    }

    @GetMapping("/createOrder")
    public R<Void> createOrder(){
        service.flashSale();
        return R.ok();
    }

}
