package com.example.demo.vo;

import lombok.Data;

/**
 * @author : Mr Li
 * @description : 设置缓存接口所需参数
 */
@Data
public class RedisPostVO {

    /**
     * 修改cookie
     */
    private String cookie;

    /**
     * 新增邮箱号
     */
    private String[] tos;
}
