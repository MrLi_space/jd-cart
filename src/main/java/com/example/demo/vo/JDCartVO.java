package com.example.demo.vo;

import lombok.Data;

/**
 * @author : Mr Li
 * @description : 解析后储存实体
 */
@Data
public class JDCartVO {

    /**
     * 商品编号
     */
    private String id;

    /**
     * 价格
     */
    private String priceShow;

    /**
     * 库存状态
     */
    private String stockState;

    /**
     * 商品名称
     */
    private String name;

}
