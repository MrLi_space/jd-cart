package com.example.demo.vo;

import lombok.Data;

/**
 * @author : Mr Li
 * @description : dd
 */
@Data
public class JavaMailVO {

    /**
     * 邮件接收方，可多人
     */
    private String[] tos;
    /**
     * 邮件主题
     */
    private String subject;
    /**
     * 邮件内容
     */
    private String content;
}
