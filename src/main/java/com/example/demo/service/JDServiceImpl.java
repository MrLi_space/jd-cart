package com.example.demo.service;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.example.demo.exception.WorldLetException;
import com.example.demo.util.GetConventUtil;
import com.example.demo.util.JavaEmailUtil;
import com.example.demo.vo.JDCartVO;
import com.example.demo.vo.JavaMailVO;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author : Mr Li
 * @description : jd
 */
@Slf4j
@Service
public class JDServiceImpl implements JDService {

    @Autowired
    private JavaEmailUtil javaEmailUtil;

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Resource(name = "coreRedisTemplate")
    private RedisTemplate<String, Object> coreRedisTemplate;

    private static final String USER_MAIL = "user:mail";
    private static final String USER_COOKIE = "jd:user:cookie";
    private static final String ACTION_1 = "https://cart.jd.com";
    private static final String FUNCTION_ID = "functionId";
    private static final String APP_ID = "appid";
    private static final String LOGIN_TYPE = "loginType";
    private static final String BODY = "body";

    public Boolean insertUserMail(String[] tos) {
        /* 检测当前缓存中的邮箱数量*/
        checkZetSize(tos.length);
        final Long add = coreRedisTemplate.opsForSet().add(USER_MAIL, tos);
        if (Objects.isNull(add) || add < 1) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    private void checkZetSize(int length) {
        final Object pop = coreRedisTemplate.opsForSet().members(USER_MAIL);
        final Set<String> pop1 = parseSet(pop);
        if (pop1.size() + length >= 200) {
            throw new WorldLetException("邮箱账号已满200位");
        }
    }

    public void insertUserCookie(String cookie) {
        coreRedisTemplate.opsForValue().set(USER_COOKIE, cookie);
    }

    @Override
    public String getRedisCookie() {
        final Object o = coreRedisTemplate.opsForValue().get(USER_COOKIE);
        if (StringUtils.isBlank(String.valueOf(o))) {
            throw new WorldLetException("请正确设置Cookie");
        }
        return String.valueOf(o);
    }

    public void resolveCart() {
        var url = "https://api.m.jd.com/api?functionId=pcCart_jc_getCurrentCart&appid=JDC_mall_cart&loginType=3&body=%7B%22serInfo%22:%7B%22area%22:%2227_2376_4343_53948%22,%22user-key%22:%22de0082cc-f145-4138-8ecc-389969d96a19%22%7D,%22cartExt%22:%7B%22specialId%22:1%7D%7D";
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(HttpHeaders.COOKIE, getRedisCookie());
        hashMap.put(HttpHeaders.ORIGIN, ACTION_1);
        hashMap.put(HttpHeaders.REFERER, ACTION_1);
        HashMap<String, Object> stringHashMap = new HashMap<>();
        stringHashMap.put(FUNCTION_ID, "pcCart_jc_getCurrentCart");
        stringHashMap.put(APP_ID, "JDC_mall_cart");
        stringHashMap.put(LOGIN_TYPE, "3");
        stringHashMap.put(BODY, "{\"serInfo\":{\"area\":\"27_2376_4343_53948\",\"user-key\":\"de0082cc-f145-4138-8ecc-389969d96a19\"},\"cartExt\":{\"specialId\":1}}");
        final String sign = GetConventUtil.sign(stringHashMap);
        var response = HttpUtil.createPost(url).headerMap(hashMap, true).form(sign).execute();
        String jsonObject = JSONUtil.toJsonStr(response.body());
        final LinkedHashMap<String, JDCartVO> map = addVO(jsonObject);
        final List<JDCartVO> list = new ArrayList<>();
        map.forEach((k, v) -> {
            if (v.getStockState().equals("有货")) {
                list.add(v);
            }
        });
        if (!list.isEmpty()) {
            final Object pop = coreRedisTemplate.opsForSet().members(USER_MAIL);
            final Set<String> strings = parseSet(pop);
            final String[] mail = strings.toArray(new String[0]);
            if (mail.length >= 1) {
                sendEmail(list, mail);
            }
        }
    }

    public void flashSale() {
        /* 每次触发-并发60次jd下单 */
        for (int i = 0; i < 60; i++) {
            threadPoolTaskExecutor.execute(this::createOrder);
        }
    }

    public void createOrder() {
        settlement();
        submitOrder();
    }

    @Override
    public void settlement() {
        var url = "https://trade.jd.com/shopping/order/getOrderInfo.action";
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(HttpHeaders.COOKIE, getRedisCookie());
        hashMap.put(HttpHeaders.REFERER, ACTION_1);
        hashMap.put(HttpHeaders.USER_AGENT, "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1");
        hashMap.put("authority", "trade.jd.com");
        hashMap.put("method", "GET");
        hashMap.put("path", "/shopping/order/getOrderInfo.action");
        hashMap.put("scheme", "https");
        HttpUtil.createGet(url).headerMap(hashMap, true).execute();
    }

    @Override
    public void submitOrder() {
        var url = "https://trade.jd.com/shopping/order/submitOrder.action?&presaleStockSign=1";
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(HttpHeaders.COOKIE, getRedisCookie());
        hashMap.put(HttpHeaders.ORIGIN, ACTION_1);
        hashMap.put(HttpHeaders.CONTENT_TYPE, ACTION_1);
        hashMap.put(HttpHeaders.REFERER, "https://trade.jd.com/shopping/order/getOrderInfo.action");
        hashMap.put(HttpHeaders.USER_AGENT, "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1");
        HashMap<String, Object> stringHashMap = new HashMap<>();
        stringHashMap.put("overseaPurchaseCookies", "");
        stringHashMap.put("vendorRemarks", "[]");
        stringHashMap.put("submitOrderParam.sopNotPutInvoice", "false");
        stringHashMap.put("submitOrderParam.trackID", "TestTrackId");
        stringHashMap.put("presaleStockSign", "1");
        stringHashMap.put("submitOrderParam.ignorePriceChange", "0");
        stringHashMap.put("submitOrderParam.btSupport", "0");
        stringHashMap.put("submitOrderParam.jxj", "1");
        stringHashMap.put("submitOrderParam.fp", "c69f77b3892861d575432167af3f6d61");
        stringHashMap.put("submitOrderParam.eid", "YWNMJHLMI7WLYX5ZRYNKEBYBORQLR7LLRALXT7KJEL2SMXDICR4CM3SR7P76BMTJS3UO6ZZPQNDYA6E4DBEJUDY7LQ");
        final String sign = GetConventUtil.sign(stringHashMap);
        HttpUtil.createPost(url).headerMap(hashMap, true).form(sign).execute();
    }

    private Set<String> parseSet(Object pop) {
        Set<String> strings = new HashSet<>();
        if (!Objects.isNull(pop) && pop instanceof Set<?>) {
            Set<?> obj = (Set<?>) pop;
            for (Object o : obj) {
                strings.add(String.valueOf(o));
            }
        }
        return strings;
    }

    private LinkedHashMap<String, JDCartVO> addVO(String jsonObject) {
        LinkedHashMap<String, JDCartVO> array = new LinkedHashMap<>();
        var status = JsonPath.read(jsonObject, "@.resultData.cartInfo");
        net.minidev.json.JSONArray status2 = JsonPath.read(status, "@.vendors");
        status2.forEach(c -> {
            net.minidev.json.JSONArray c2 = JsonPath.read(c, "@.sorted");
            c2.forEach(cc -> {
                try {
                    String name = JsonPath.read(cc, "@.item.Name");
                    if (Objects.isNull(name)) {
                        net.minidev.json.JSONArray objects = JsonPath.read(cc, "@.item.items");
                        objects.forEach(c3 -> {
                            String name2 = JsonPath.read(c3, "@.item.Name");
                            resolve(array, c3, name2);
                        });
                    } else {
                        resolve(array, cc, name);
                    }
                } catch (Exception e) {
                    log.error("-解析异常- 当前解析内容{}", cc);
                }
            });
        });
        return array;
    }

    private void resolve(LinkedHashMap<String, JDCartVO> array, Object c3, String name2) {
        if (name2.length() >= 20) {
            name2 = name2.substring(0, 30);
        }
        String priceShow = JsonPath.read(c3, "@.item.PriceShow");
        String stockState = JsonPath.read(c3, "@.item.stockState");
        String id = JsonPath.read(c3, "@.item.Id").toString();
        JDCartVO vo = new JDCartVO();
        vo.setId(id);
        vo.setPriceShow(priceShow);
        vo.setStockState(stockState);
        vo.setName(name2);
        array.put(id, vo);
    }

    /**
     * 　发送当前有货商品信息邮件
     *
     * @param vo        商品信息
     * @param acceptors 邮件接受人
     */
    private void sendEmail(List<JDCartVO> vo, String[] acceptors) {
        sendMail(vo, acceptors);
    }

    private void sendMail(List<JDCartVO> vo, String[] strings) {
        var postVO = new JavaMailVO();
        var date = new Date();
        postVO.setSubject("当前有货-商品ID清单-当前时间:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
        StringBuilder content = new StringBuilder();
        vo.forEach(c2 -> content.append("商品名称:").append(c2.getName()).append("\t搜索商品ID号立即到达商品主页\t").append(c2.getId()).append("\n"));
        postVO.setContent(content.toString());
        for (String string : strings) {
            threadPoolTaskExecutor.execute(() -> {
                postVO.setTos(new String[]{string});
                javaEmailUtil.commonEmail(postVO);
            });
        }
    }
}
