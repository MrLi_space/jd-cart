package com.example.demo.service;

/**
 * @author : Mr Li
 * @description : jd
 */
public interface JDService {

    /**
     * 将邮箱地址新增
     * @param tos 当前新增邮箱数组
     * @return 邮箱池是否已满
     */
    Boolean insertUserMail(String[] tos);

    /**
     * 更新用户cookie
     * @param cookie 浏览器cookie
     */
    void insertUserCookie(String cookie);

    String getRedisCookie();

    /**
     * 解析查询购物车返回的JSON-DATA
     */
    void resolveCart();

    /**
     * jd-去结算
     */
    void settlement();

    /**
     * 提交订单
     */
    void submitOrder();
}
