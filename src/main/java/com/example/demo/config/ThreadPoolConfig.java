package com.example.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @author : Mr Li
 * @description : 线程池配置
 */
@Slf4j
@Configuration
public class ThreadPoolConfig {

    private static final String THREAD_NAME_PREFIX = "jd_cart-";

    @Bean("threadPoolTaskExecutor")
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        final int cpuNum = Runtime.getRuntime().availableProcessors();
        // 线程池中所保存的核心线程数
        final int corePoolSize = cpuNum + 1;
        // 池中允许的最大线程数
        final int maxPoolSize = corePoolSize * 2;
        // 使用spring线程池
        var taskExecutor = new ThreadPoolTaskExecutor();
        // 线程池维护线程的最少数量
        taskExecutor.setCorePoolSize(corePoolSize);
        // 线程池维护线程的最大数量
        taskExecutor.setMaxPoolSize(maxPoolSize);
        // 线程池维护线程所允许的空闲时间
        taskExecutor.setKeepAliveSeconds(0);
        //配置线程池中的线程的名称前缀
        taskExecutor.setThreadNamePrefix(THREAD_NAME_PREFIX);
        // 执行初始化
        taskExecutor.initialize();
        log.info("线程池初始化成功...");
        return taskExecutor;
    }
}