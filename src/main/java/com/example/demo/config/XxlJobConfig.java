package com.example.demo.config;

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ：Mr Li
 * @description ：xxl-job配置类
 */
@Slf4j
@Configuration
public class XxlJobConfig {
    @Value("xxl.job.admin.address")
    private String xxlJobAdminAddress;

    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        log.info(">>>>>>>>>>> xxl-job config init.");
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(xxlJobAdminAddress);
        xxlJobSpringExecutor.setAppname("schedule-order-task-server");
        xxlJobSpringExecutor.setPort(9900);
        xxlJobSpringExecutor.setLogPath("/tmp/logs/");
        xxlJobSpringExecutor.setLogRetentionDays(1);
        return xxlJobSpringExecutor;
    }

}